
```{post} 2023-07-18
:author: "@sdql"
:tags: participante
:category: miembros
:language: Español, Inglés
:location: Colombia
:excerpt: 1
```

# @sdql's Space

¡Hola soy `@sdql`! Actualmente soy estudiante de programación de software, soy un apasionado por la tecnología y los
idiomas. Soy una persona que siempre busca aprender nuevas cosas y me gusta siempre compartir mi conocimiento con los
demás.

¡Éste es mi espacio en GuayaHack!

## Blog

```{postlist}
:author: "@sdql"
:category: blog
:excerpts:
:sort:
```

